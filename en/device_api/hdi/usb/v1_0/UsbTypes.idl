/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief Provides unified, standard USB driver APIs to implement USB driver access.
 *
 * Using the APIs provided by the USB driver module, USB service developers can implement the following functions: 
 * enabling/disabling a device, obtaining a device descriptor, obtaining a file descriptor, enabling/disabling an interface, 
 * reading/writing data in bulk mode, setting or obtaining device functions, and binding or unbinding a subscriber.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file UsbTypes.idl
 *
 * @brief Defines the data types used in USB driver module APIs.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the USB driver module APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.usb.v1_0;

/**
 * @brief Defines port information of a USB device.
 *
 * @since 3.2
 * @version 1.0
 */
struct PortInfo {
    /** Port ID of the USB device */
    int portId;
    /** Power role of the USB device */
    int powerRole;
    /** Data role of the USB device */
    int dataRole;
    /** USB device mode */
    int mode;
};

/**
 * @brief Defines address information of a USB device.
 *
 * @since 3.2
 * @version 1.0
 */
struct UsbDev {
    /** Bus number of the USB device */
    unsigned char busNum;
    /** USB device address */
    unsigned char devAddr;
};

/**
 * @brief Defines pipe information of a USB device.
 *
 * @since 3.2
 * @version 1.0
 */
struct UsbPipe {
    /** USB interface ID */
    unsigned char intfId;
    /** USB endpoint ID */
    unsigned char endpointId;
};

/**
 * @brief Defines control transfer information of a USB device.
 *
 * @since 3.2
 * @version 1.0
 */
struct UsbCtrlTransfer {
    /** Request type */
    int requestType;
    /** Request command */
    int requestCmd;
    /** Request value */
    int value;
    /** Index */
    int index;
    /** Timeout interval */
    int timeout;
};

/**
 * @brief Defines USB device information.
 *
 * @since 3.2
 * @version 1.0
 */
struct USBDeviceInfo {
    /** USB device status */
    int status;
    /** Bus number of the USB device */
    int busNum;
    /** USB device ID */
    int devNum;
};
/** @} */
