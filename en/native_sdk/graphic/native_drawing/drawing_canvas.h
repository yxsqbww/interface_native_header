/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_H
#define C_INCLUDE_DRAWING_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_canvas.h
 *
 * @brief Declares the functions related to the canvas in the drawing module.
 *
 * File to include: native_drawing/drawing_canvas.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Canvas</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Canvas</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Canvas* OH_Drawing_CanvasCreate(void);

/**
 * @brief Destroys an <b>OH_Drawing_Canvas</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDestroy(OH_Drawing_Canvas*);

/**
 * @brief Binds a bitmap to a canvas so that the content drawn on the canvas is output to the bitmap.
 * (This process is called CPU rendering.)
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasBind(OH_Drawing_Canvas*, OH_Drawing_Bitmap*);

/**
 * @brief Attaches a pen to a canvas so that the canvas can use the style and color of the pen to outline a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Pen Pointer to an <b>OH_Drawing_Pen</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachPen(OH_Drawing_Canvas*, const OH_Drawing_Pen*);

/**
 * @brief Detaches the pen from a canvas so that the canvas can no longer use the style and color of the pen
 * to outline a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachPen(OH_Drawing_Canvas*);

/**
 * @brief Attaches a brush to a canvas so that the canvas can use the style and color of the brush to fill in a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Brush Pointer to an <b>OH_Drawing_Brush</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasAttachBrush(OH_Drawing_Canvas*, const OH_Drawing_Brush*);

/**
 * @brief Detaches the brush from a canvas so that the canvas can no longer use the style and color of the brush
 * to fill in a shape.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDetachBrush(OH_Drawing_Canvas*);

/**
 * @brief Saves the current canvas status (canvas matrix) to the top of the stack.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasSave(OH_Drawing_Canvas*);

/**
 * @brief Restores the canvas status (canvas matrix) saved on the top of the stack.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasRestore(OH_Drawing_Canvas*);

/**
 * @brief Obtains the number of canvas statuses (canvas matrices) saved in the stack.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @return Returns a 32-bit value that describes the number of canvas statuses (canvas matrices).
 * @since 11
 * @version 1.0
 */
uint32_t OH_Drawing_CanvasGetSaveCount(OH_Drawing_Canvas*);

/**
 * @brief Restores to a given number of canvas statuses (canvas matrices).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param saveCount Number of canvas statuses (canvas matrices).
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRestoreToCount(OH_Drawing_Canvas*, uint32_t saveCount);

/**
 * @brief Draws a line segment.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param x1 X coordinate of the start point of the line segment.
 * @param y1 Y coordinate of the start point of the line segment.
 * @param x2 X coordinate of the end point of the line segment.
 * @param y2 Y coordinate of the end point of the line segment.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawLine(OH_Drawing_Canvas*, float x1, float y1, float x2, float y2);

/**
 * @brief Draws a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasDrawPath(OH_Drawing_Canvas*, const OH_Drawing_Path*);

/**
 * @brief Draws a bitmap. A bitmap, also referred to as a dot matrix image, a pixel map image, or a grid image,
 * includes single points called pixels (image elements).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Bitmap Pointer to an <b>OH_Drawing_Bitmap</b> object.
 * @param left X coordinate of the upper left corner of the bitmap.
 * @param top Y coordinate of the upper left corner of the bitmap.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawBitmap(OH_Drawing_Canvas*, const OH_Drawing_Bitmap*, float left, float top);

/**
 * @brief Draws a rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief Draws a circle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Point Pointer to an <b>OH_Drawing_Point</b> object, which indicates the center of the circle.
 * @param radius Radius of the circle.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawCircle(OH_Drawing_Canvas*, const OH_Drawing_Point*, float radius);

/**
 * @brief Draws an oval.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawOval(OH_Drawing_Canvas*, const OH_Drawing_Rect*);

/**
 * @brief Draws an arc.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param startAngle Start angle of the arc.
 * @param sweepAngle Sweep angle of the arc.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawArc(OH_Drawing_Canvas*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief Draws a rounded rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_RoundRect Pointer to an <b>OH_Drawing_RoundRect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawRoundRect(OH_Drawing_Canvas*, const OH_Drawing_RoundRect*);

/**
 * @brief Draws a text blob.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_TextBlobPointer to an <b>OH_Drawing_TextBlob</b> object.
 * @param x X coordinate of the lower left corner of the text blob.
 * @param y Y coordinate of the lower left corner of the text blob.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasDrawTextBlob(OH_Drawing_Canvas*, const OH_Drawing_TextBlob*, float x, float y);

/**
 * @brief Enumerates the canvas clipping modes.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /**
     * Clips a specified area. That is, the difference set is obtained.
     */
    DIFFERENCE,
    /**
     * Retains a specified area. That is, the intersection is obtained.
     */
    INTERSECT,
} OH_Drawing_CanvasClipOp;

/**
 * @brief Clips a rectangle.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Rect Pointer to an <b>OH_Drawing_Rect</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipRect(OH_Drawing_Canvas*, const OH_Drawing_Rect*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Clips a path.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param clipOp Clip mode. For details about the available options, see @{link OH_Drawing_CanvasClipOp}.
 * @param doAntiAlias Whether to enable anti-aliasing. The value <b>true</b> means to enable anti-aliasing,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasClipPath(OH_Drawing_Canvas*, const OH_Drawing_Path*,
    OH_Drawing_CanvasClipOp clipOp, bool doAntiAlias);

/**
 * @brief Rotates a canvas by a given angle. A positive number indicates clockwise rotation, and a negative number
 * indicates clockwise rotation.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param degrees Angle to rotate.
 * @param px X coordinate of the rotation center.
 * @param py Y coordinate of the rotation center.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasRotate(OH_Drawing_Canvas*, float degrees, float px, float py);

/**
 * @brief Translates a canvas by a given distance.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param dx Distance to translate in the horizontal direction.
 * @param dy Distance to translate in the vertical direction.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasTranslate(OH_Drawing_Canvas*, float dx, float dy);

/**
 * @brief Scales a canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param sx Horizontal scale factor.
 * @param sy Vertical scale factor.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_CanvasScale(OH_Drawing_Canvas*, float sx, float sy);

/**
 * @brief Clears a canvas by using a given color.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object.
 * @param color Color, which is a 32-bit (ARGB) variable.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_CanvasClear(OH_Drawing_Canvas*, uint32_t color);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
