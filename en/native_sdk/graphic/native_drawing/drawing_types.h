/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TYPES_H
#define C_INCLUDE_DRAWING_TYPES_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_types.h
 *
 * @brief Declares the data types of the canvas, brush, pen, bitmap, and path used to draw 2D graphics.
 *
 * File to include: native_drawing/drawing_types.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a rectangular canvas on which various shapes, images, and texts can be drawn
 * by using the brush and pen.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Canvas OH_Drawing_Canvas;

/**
 * @brief Defines a pen, which is used to describe the style and color to outline a shape.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Pen OH_Drawing_Pen;

/**
 * @brief Defines a brush, which is used to describe the style and color to fill in a shape.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Brush OH_Drawing_Brush;

/**
 * @brief Defines a path, which is used to customize various shapes.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Path OH_Drawing_Path;

/**
 * @brief Defines a bitmap, which is a memory area that contains the pixel data of a shape.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Bitmap OH_Drawing_Bitmap;

/**
 * @brief Defines a coordinate point.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Point OH_Drawing_Point;

/**
 * @brief Defines a rectangle.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Rect OH_Drawing_Rect;

/**
 * @brief Defines a rounded rectangle.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_RoundRect OH_Drawing_RoundRect;

/**
 * @brief Defines a matrix, which is used to describe coordinate transformation.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Matrix OH_Drawing_Matrix;

/**
 * @brief Defines a shader effect, which is used to describe the source color of the drawn content.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_ShaderEffect OH_Drawing_ShaderEffect;

/**
 * @brief Defines a filter, which consists of a color filter and mask filter.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Filter OH_Drawing_Filter;

/**
 * @brief Defines a mask filter, which is used to convert a mask into a new one.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_MaskFilter OH_Drawing_MaskFilter;

/**
 * @brief Defines a color filter, which is used to convert a color into a new one.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_ColorFilter OH_Drawing_ColorFilter;

/**
 * @brief Defines a font.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Font OH_Drawing_Font;

/**
 * @brief Defines a typeface.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Typeface OH_Drawing_Typeface;

/**
 * @brief Defines a text blob, which is an immutable container that holds multiple texts.
 * Each text line consists of a glyph and position.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_TextBlob OH_Drawing_TextBlob;

/**
 * @brief Defines a text blob builder, which is used to build a text blob.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_TextBlobBuilder OH_Drawing_TextBlobBuilder;

/**
 * @brief Enumerates the storage formats of bitmap pixels.
 * 
 * @since 8
 * @version 1.0
 */
typedef enum {
    /** Unknown format. */
    COLOR_FORMAT_UNKNOWN,
    /** Each pixel is represented by 8 bits, which together indicate alpha. */
    COLOR_FORMAT_ALPHA_8,
    /**
     * Each pixel is represented by 16 bits. From the most significant bit to the least significant bit,
     * the first 5 bits indicate red, the subsequent 6 bits indicate green, and the last 5 bits indicate blue.
     */
    COLOR_FORMAT_RGB_565,
    /**
     * Each pixel is represented by 16 bits. From the most significant bit to the least significant bit,
     * every 4 bits indicate alpha, red, green, and blue, respectively.
     */
    COLOR_FORMAT_ARGB_4444,
    /**
     * Each pixel is represented by 32 bits. From the most significant bit to the least significant bit,
     * every 8 bits indicate alpha, red, green, and blue, respectively.
     */
    COLOR_FORMAT_RGBA_8888,
    /**
     * Each pixel is represented by 32 bits. From the most significant bit to the least significant bit,
     * every 8 bits indicate blue, green, red, and alpha, respectively.
     */
    COLOR_FORMAT_BGRA_8888
} OH_Drawing_ColorFormat;

/**
 * @brief Enumerates the alpha formats of bitmap pixels.
 *
 * @since 8
 * @version 1.0
 */
typedef enum {
    /** Unknown format. */
    ALPHA_FORMAT_UNKNOWN,
    /** The bitmap does not have the alpha component. */
    ALPHA_FORMAT_OPAQUE,
    /** The color component of each pixel is premultiplied by the alpha component. */
    ALPHA_FORMAT_PREMUL,
    /** The color component of each pixel is not premultiplied by the alpha component. */
    ALPHA_FORMAT_UNPREMUL
} OH_Drawing_AlphaFormat;

/**
 * @brief Enumerates the blend modes. In blend mode, each operation generates a new color from two colors
 * (source color and target color).
 * These operations are the same on the four channels (red, green, blue, and alpha).
 * The operations for the alpha channel are used as examples.
 *
 * For brevity, the following abbreviations are used:
 *
 * <b>s</b>: source.
 *
 * <b>d</b>: destination.
 *
 * <b>sa</b>: source alpha.
 *
 * <b>da</b>: destination alpha.
 *
 * The following abbreviations are used in the calculation result:
 *
 * <b>r</b>: The calculation methods of the four channels are the same.
 *
 * <b>ra</b>: Only the alpha channel is manipulated.
 *
 * <b>rc</b>: The other three color channels are manipulated.
 *
 * @since 11
 * @version 1.0
 */
typedef enum {
    /** Clear mode. r = 0. */
    BLEND_MODE_CLEAR,
    /**
     * r = s (The four channels of <b>result</b> are equal to the four channels of <b>source</b>, that is,
     * the result is equal to the source.)
     */
    BLEND_MODE_SRC,
    /**
     * r = d (The four channels of <b>result</b> are equal to the four channels of <b>destination</b>, that is,
     * the result is equal to the destination.)
     */
    BLEND_MODE_DST,
    /** r = s + (1 - sa) * d. */
    BLEND_MODE_SRC_OVER,
    /** r = d + (1 - da) * s. */
    BLEND_MODE_DST_OVER,
    /** r = s * da. */
    BLEND_MODE_SRC_IN,
    /** r = d * sa. */
    BLEND_MODE_DST_IN,
    /** r = s * (1 - da). */
    BLEND_MODE_SRC_OUT,
    /** r = d * (1 - sa). */
    BLEND_MODE_DST_OUT,
    /** r = s * da + d * (1 - sa). */
    BLEND_MODE_SRC_ATOP,
    /** r = d * sa + s * (1 - da). */
    BLEND_MODE_DST_ATOP,
    /** r = s * (1 - da) + d * (1 - sa). */
    BLEND_MODE_XOR,
    /** r = min(s + d, 1). */
    BLEND_MODE_PLUS,
    /** r = s * d. */
    BLEND_MODE_MODULATE,
    /** Screen mode. r = s + d - s \* d */
    BLEND_MODE_SCREEN,
    /** Overlay mode. */
    BLEND_MODE_OVERLAY,
    /** Darken mode. rc = s + d - max(s \* da, d \* sa), ra = s + (1 - sa) \* d */
    BLEND_MODE_DARKEN,
    /** Lighten mode. rc = s + d - min(s \* da, d \* sa), ra = s + (1 - sa) \* d */
    BLEND_MODE_LIGHTEN,
    /** Color dodge mode. */
    BLEND_MODE_COLOR_DODGE,
    /** Color burn mode. */
    BLEND_MODE_COLOR_BURN,
    /** Hard light mode. */
    BLEND_MODE_HARD_LIGHT,
    /** Soft light mode. */
    BLEND_MODE_SOFT_LIGHT,
    /** Difference mode. rc = s + d - 2 \* (min(s \* da, d \* sa)), ra = s + (1 - sa) \* d */
    BLEND_MODE_DIFFERENCE,
    /** Exclusion mode. rc = s + d - two(s \* d), ra = s + (1 - sa) \* d */
    BLEND_MODE_EXCLUSION,
    /** Multiply mode. r = s \* (1 - da) + d \* (1 - sa) + s \* d */
    BLEND_MODE_MULTIPLY,
    /** Hue mode. */
    BLEND_MODE_HUE,
    /** Saturation mode. */
    BLEND_MODE_SATURATION,
    /** Color mode. */
    BLEND_MODE_COLOR,
    /** Luminosity mode. */
    BLEND_MODE_LUMINOSITY,
} OH_Drawing_BlendMode;

#ifdef __cplusplus
}
#endif
/** @} */
#endif
