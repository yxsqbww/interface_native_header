/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H
#define C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_typography.h
 *
 * @brief 定义绘制模块中排版相关的函数。
 *
 * 引用文件"native_drawing/drawing_text_typography.h"
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_canvas.h"
#include "drawing_color.h"
#include "drawing_font.h"
#include "drawing_text_declaration.h"
#include "drawing_types.h"

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 文字方向
 */
enum OH_Drawing_TextDirection {
    /** 方向：从右到左 */
    TEXT_DIRECTION_RTL,
    /** 方向：从左到右 */
    TEXT_DIRECTION_LTR,
};

/**
 * @brief 文字对齐方式
 */
enum OH_Drawing_TextAlign {
    /** 左对齐 */
    TEXT_ALIGN_LEFT,
    /** 右对齐 */
    TEXT_ALIGN_RIGHT,
    /** 居中对齐 */
    TEXT_ALIGN_CENTER,
    /**
     * 两端对齐，即紧靠左和右边缘，中间单词空隙由空格填充，最后一行除外。
     */
    TEXT_ALIGN_JUSTIFY,
    /**
     * 当OH_Drawing_TextDirection是TEXT_DIRECTION_LTR时，
     * TEXT_ALIGN_START和TEXT_ALIGN_LEFT相同；
     * 类似地，当OH_Drawing_TextDirection是TEXT_DIRECTION_RTL时，
     * TEXT_ALIGN_START和TEXT_ALIGN_RIGHT相同。
     */
    TEXT_ALIGN_START,
    /**
     * 当OH_Drawing_TextDirection是TEXT_DIRECTION_LTR时，
     * TEXT_ALIGN_END和TEXT_ALIGN_RIGHT相同；
     * 类似地，当OH_Drawing_TextDirection是TEXT_DIRECTION_RTL时，
     * TEXT_ALIGN_END和TEXT_ALIGN_LEFT相同。
     */
    TEXT_ALIGN_END,
};

/**
 * @brief 字重
 */
enum OH_Drawing_FontWeight {
    /** 字重为thin */
    FONT_WEIGHT_100,
    /** 字重为extra-light */
    FONT_WEIGHT_200,
    /** 字重为light */
    FONT_WEIGHT_300,
    /** 字重为normal/regular */
    FONT_WEIGHT_400,
    /** 字重为medium */
    FONT_WEIGHT_500,
    /** 字重为semi-bold */
    FONT_WEIGHT_600,
    /** 字重为bold */
    FONT_WEIGHT_700,
    /** 字重为extra-bold */
    FONT_WEIGHT_800,
    /** 字重为black */
    FONT_WEIGHT_900,
};

/**
 * @brief 基线位置
 */
enum OH_Drawing_TextBaseline {
    /** 用于表音文字，基线在中间偏下的位置 */
    TEXT_BASELINE_ALPHABETIC,
    /** 用于表意文字，基线位于底部 */
    TEXT_BASELINE_IDEOGRAPHIC,
};

/**
 * @brief 文本装饰
 */
enum OH_Drawing_TextDecoration {
    /** 无装饰 */
    TEXT_DECORATION_NONE = 0x0,
    /** 下划线 */
    TEXT_DECORATION_UNDERLINE = 0x1,
    /** 上划线 */
    TEXT_DECORATION_OVERLINE = 0x2,
    /** 删除线 */
    TEXT_DECORATION_LINE_THROUGH = 0x4,
};

/**
 * @brief 区分字体是否为斜体
 */
enum OH_Drawing_FontStyle {
    /** 非斜体 */
    FONT_STYLE_NORMAL,
    /** 斜体 */
    FONT_STYLE_ITALIC,
};

/**
 * @brief 占位符垂直对齐枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_PlaceholderVerticalAlignment {
    /** 偏移于基线对齐 */
    ALIGNMENT_OFFSET_AT_BASELINE,
    /** 高于基线对齐 */
    ALIGNMENT_ABOVE_BASELINE,
    /** 低于基线对齐 */
    ALIGNMENT_BELOW_BASELINE,
    /** 行框顶部对齐 */
    ALIGNMENT_TOP_OF_ROW_BOX,
    /** 行框底部对齐 */
    ALIGNMENT_BOTTOM_OF_ROW_BOX,
    /** 行框中心对齐 */
    ALIGNMENT_CENTER_OF_ROW_BOX,
} OH_Drawing_PlaceholderVerticalAlignment;

/**
 * @brief 用于描述位占位符跨度的结构体
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_PlaceholderSpan {
    /** 占位符宽度 */
    double width;
    /** 占位符高度 */
    double height;
    /** 占位符对齐方式 */
    OH_Drawing_PlaceholderVerticalAlignment alignment;
    /** 占位符基线 */
    OH_Drawing_TextBaseline baseline;
    /** 占位符基线偏移 */
    double baselineOffset;
} OH_Drawing_PlaceholderSpan;

/**
 * @brief 文本装饰样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_TextDecorationStyle {
    /** 实心样式 */
    TEXT_DECORATION_STYLE_SOLID,
    /** 双重样式 */
    TEXT_DECORATION_STYLE_DOUBLE,
    /** 圆点样式 */
    TEXT_DECORATION_STYLE_DOTTED,
    /** 虚线样式 */
    TEXT_DECORATION_STYLE_DASHED,
    /** 波浪样式 */
    TEXT_DECORATION_STYLE_WAVY,
} OH_Drawing_TextDecorationStyle;

/**
 * @brief 省略号样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_EllipsisModal {
    /** 头部模式，即省略号放在文本头部 */
    ELLIPSIS_MODAL_HEAD = 0,
    /** 中部模式，即省略号放在文本中部 */
    ELLIPSIS_MODAL_MIDDLE = 1,
    /** 尾部模式，即省略号放在文本尾部 */
    ELLIPSIS_MODAL_TAIL = 2,
} OH_Drawing_EllipsisModal;

/**
 * @brief 文本的中断策略枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_BreakStrategy {
    /** 贪心策略，换行时尽可能填满每一行 */
    BREAK_STRATEGY_GREEDY = 0,
    /** 高质量策略，换行时优先考虑文本的连续性 */
    BREAK_STRATEGY_HIGH_QUALITY = 1,
    /** 平衡策略，换行时在单词边界换行 */
    BREAK_STRATEGY_BALANCED = 2,
} OH_Drawing_BreakStrategy;

/**
 * @brief 单词的断词方式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_WordBreakType {
    /** 常规方式 */
    WORD_BREAK_TYPE_NORMAL = 0,
    /** 全部中断方式 */
    WORD_BREAK_TYPE_BREAK_ALL = 1,
    /** 单词中断方式 */
    WORD_BREAK_TYPE_BREAK_WORD = 2,
} OH_Drawing_WordBreakType;

/**
 * @brief 矩形框高度样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_RectHeightStyle {
    /** 紧密样式 */
    RECT_HEIGHT_STYLE_TIGHT,
    /** 最大样式 */
    RECT_HEIGHT_STYLE_MAX,
    /** 包含行间距中间样式 */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEMIDDLE,
    /** 包含行间距顶部样式 */
    RECT_HEIGHT_STYLE_INCLUDELINESPACETOP,
    /** 包含行间距底部样式 */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEBOTTOM,
    /** 结构样式 */
    RECT_HEIGHT_STYLE_STRUCT,
} OH_Drawing_RectHeightStyle;

/**
 * @brief 矩形框宽度样式枚举
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_RectWidthStyle {
    /** 紧密样式 */
    RECT_WIDTH_STYLE_TIGHT,
    /** 最大样式 */
    RECT_WIDTH_STYLE_MAX,
} OH_Drawing_RectWidthStyle;

/**
 * @brief 描述系统字体详细信息的结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontDescriptor {
    /** 系统字体的文件路径 */
    char* path;
    /** 唯一标识字体的名称 */
    char* postScriptName;
    /** 系统字体的名称 */
    char* fullName;
    /** 系统字体的字体家族 */
    char* fontFamily;
    /** 系统字体的子字体家族 */
    char* fontSubfamily;
    /** 系统字体的粗细程度 */
    int weight;
    /** 系统字体的宽窄风格属性 */
    int width;
    /** 系统字体倾斜度 */
    int italic;
    /** 系统字体是否紧凑，true表示字体紧凑，false表示字体非紧凑 */
    bool monoSpace;
    /** 系统字体是否支持符号字体，true表示支持符号字体，false表示不支持符号字体 */
    bool symbolic;
} OH_Drawing_FontDescriptor;

/**
 * @brief 文字行位置信息。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_LineMetrics {
    /** 文字相对于基线以上的高度 */
    double ascender;
    /** 文字相对于基线以下的高度 */
    double descender;
    /** 大写字母的高度 */
    double capHeight;
    /** 小写字母的高度 */
    double xHeight;
    /** 文字宽度 */
    double width;
    /** 行高 */
    double height;
    /** 文字左端到容器左端距离，左对齐为0，右对齐为容器宽度减去行文字宽度 */
    double x;
    /** 文字上端到容器上端高度，第一行为0，第二行为第一行高度 */
    double y;
    /** 行起始位置字符索引 */
    size_t startIndex;
    /** 行结束位置字符索引 */
    size_t endIndex;
    /** 第一个字的度量信息 */
    OH_Drawing_Font_Metrics firstCharMetrics;
} OH_Drawing_LineMetrics;

/**
 * @brief 创建指向OH_Drawing_TypographyStyle对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的OH_Drawing_TypographyStyle对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyStyle* OH_Drawing_CreateTypographyStyle(void);

/**
 * @brief 释放被OH_Drawing_TypographyStyle对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief 设置文本方向。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置文本方向，设置1为从左到右，设置0或其它为从右到左，具体可见{@link OH_Drawing_TextDirection}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextDirection(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextDirection */);

/**
 * @brief 设置文本对齐方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置文本对齐方式，设置1为右对齐，设置2为居中对齐，设置3为两端对齐，设置4为与文字方向相同，设置5为文字方向相反，设置0或其它为左对齐，具体可见{@link OH_Drawing_TextAlign}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextAlign(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextAlign */);

/**
 * @brief 获取文字对齐方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文字对齐方式。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_TypographyGetEffectiveAlignment(OH_Drawing_TypographyStyle* style);

/**
 * @brief 设置文本最大行数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 最大行数。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextMaxLines(OH_Drawing_TypographyStyle*, int /* maxLines */);

/**
 * @brief 创建指向OH_Drawing_TextStyle对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的OH_Drawing_TextStyle对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_CreateTextStyle(void);

/**
 * @brief 获取字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回指向字体风格对象{@link OH_Drawing_TextStyle}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_TypographyGetTextStyle(OH_Drawing_TypographyStyle* style);

/**
 * @brief 释放被OH_Drawing_TextStyle对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTextStyle(OH_Drawing_TextStyle*);

/**
 * @brief 设置文本颜色。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param uint32_t 颜色。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief 设置字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 字号。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontSize(OH_Drawing_TextStyle*, double /* fontSize */);

/**
 * @brief 设置字重。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置字重，设置0字重为thin，设置1字重为extra-light，设置2字重为light，设置4字重为medium，设置5字重为semi-bold，
 * 设置6字重为bold，设置7字重为extra-bold，设置8字重为black，设置3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontWeight(OH_Drawing_TextStyle*, int /* OH_Drawing_FontWeight */);

/**
 * @brief 设置字体基线位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置字体基线位置，设置1基线位于底部，设置0或其它基线在中间偏下的位置，具体可见{@link OH_Drawing_TextBaseline}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBaseLine(OH_Drawing_TextStyle*, int /* OH_Drawing_TextBaseline */);

/**
 * @brief 设置装饰。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置装饰，设置1为下划线，设置2为上划线，设置3为删除线，设置0或其它为无装饰，具体可见{@link OH_Drawing_TextDecoration}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief 设置装饰颜色。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param uint32_t 颜色。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief 设置字体高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 字体高度。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontHeight(OH_Drawing_TextStyle*, double /* fontHeight */);

/**
 * @brief 设置字体类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 字体名称数量。
 * @param char 指向字体类型的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontFamilies(OH_Drawing_TextStyle*,
    int /* fontFamiliesNumber */, const char* fontFamilies[]);

/**
 * @brief 设置字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置字体风格，设置1为斜体，设置0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontStyle(OH_Drawing_TextStyle*, int /* OH_Drawing_FontStyle */);

/**
 * @brief 设置语言区域。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param char 语言区域，数据类型为指向char的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLocale(OH_Drawing_TextStyle*, const char*);

/**
 * @brief 设置前景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleForegroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 返回设置的前景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetForegroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 设置前景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleForegroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 返回设置的前景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetForegroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 设置背景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBackgroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 返回设置的背景色画刷。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Brush 指向画刷{@link OH_Drawing_Brush}对象的指针，由{@link OH_Drawing_BrushCreate}获取。
 * @since 12
 * @version 1.0
 */
 void OH_Drawing_TextStyleGetBackgroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief 设置背景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBackgroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 返回设置的背景色画笔。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格{@link OH_Drawing_TextStyle}对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Pen 指向画笔{@link OH_Drawing_Pen}对象的指针，由{@link OH_Drawing_PenCreate}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetBackgroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief 创建指向OH_Drawing_TypographyCreate对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param OH_Drawing_FontCollection 指向OH_Drawing_FontCollection的指针，由{@link OH_Drawing_CreateFontCollection}获取。
 * @return 指向新创建的OH_Drawing_TypographyCreate对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyCreate* OH_Drawing_CreateTypographyHandler(OH_Drawing_TypographyStyle*,
    OH_Drawing_FontCollection*);

/**
 * @brief 释放被OH_Drawing_TypographyCreate对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyHandler(OH_Drawing_TypographyCreate*);

/**
 * @brief 设置排版风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPushTextStyle(OH_Drawing_TypographyCreate*, OH_Drawing_TextStyle*);

/**
 * @brief 设置文本内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param char 指向文本内容的指针。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddText(OH_Drawing_TypographyCreate*, const char*);

/**
 * @brief 排版弹出。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPopTextStyle(OH_Drawing_TypographyCreate*);

/**
 * @brief 创建指向OH_Drawing_Typography对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @return 指向OH_Drawing_Typography对象的指针。
 * @since 8
 * @version 1.0
 */
OH_Drawing_Typography* OH_Drawing_CreateTypography(OH_Drawing_TypographyCreate*);

/**
 * @brief 释放OH_Drawing_Typography对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypography(OH_Drawing_Typography*);

/**
 * @brief 排版布局。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param double 文本最大宽度
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyLayout(OH_Drawing_Typography*, double /* maxWidth */);

/**
 * @brief 显示文本。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param OH_Drawing_Canvas 指向OH_Drawing_Canvas对象的指针，由OH_Drawing_CanvasCreate()获取。
 * @param double x坐标。
 * @param double y坐标。
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyPaint(OH_Drawing_Typography*, OH_Drawing_Canvas*,
    double /* potisionX */, double /* potisionY */);

/**
 * @brief 获取最大宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最大宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxWidth(OH_Drawing_Typography*);

/**
 * @brief 获取高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回高度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetHeight(OH_Drawing_Typography*);

/**
 * @brief 获取最长行的宽度，建议实际使用时将返回值向上取整。当文本内容为空时，返回float的最小值，
 * 即：-340282346638528859811704183484516925440.000000。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最长行的宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetLongestLine(OH_Drawing_Typography*);

/**
 * @brief 获取最小固有宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最小固有宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMinIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief 获取最大固有宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回最大固有宽度。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief 获取字母文字基线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回字母文字基线。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetAlphabeticBaseline(OH_Drawing_Typography*);

/**
 * @brief 获取表意文字基线。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回表意文字基线。
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetIdeographicBaseline(OH_Drawing_Typography*);

/**
 * @brief 设置占位符。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate 指向OH_Drawing_TypographyCreate对象的指针，由{@link OH_Drawing_CreateTypographyHandler}获取。
 * @param OH_Drawing_PlaceholderSpan 指向OH_Drawing_PlaceholderSpan对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddPlaceholder(OH_Drawing_TypographyCreate*, OH_Drawing_PlaceholderSpan*);

/**
 * @brief 获取文本是否超过最大行。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回文本是否超过最大行，true表示超过，false表示未超过。
 * @since 11
 * @version 1.0
 */
bool OH_Drawing_TypographyDidExceedMaxLines(OH_Drawing_Typography*);

/**
 * @brief 获取指定范围内的文本框。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param size_t 设置开始位置。
 * @param size_t 设置结束位置。
 * @param OH_Drawing_RectHeightStyle 设置高度样式，支持可选的高度样式具体可见{@link OH_Drawing_RectHeightStyle}枚举。
 * @param OH_Drawing_RectWidthStyle 设置宽度样式，支持可选的宽度样式具体可见{@link OH_Drawing_RectWidthStyle}枚举。
 * @return 返回指定范围内的文本框，具体可见{@link OH_Drawing_TextBox}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForRange(OH_Drawing_Typography*,
    size_t, size_t, OH_Drawing_RectHeightStyle, OH_Drawing_RectWidthStyle);

/**
 * @brief 获取占位符的文本框。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回占位符的文本框，返回类型为{@link OH_Drawing_TextBox}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForPlaceholders(OH_Drawing_Typography*);

/**
 * @brief 获取文本框左侧位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框左侧位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetLeftFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框右侧位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框右侧位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetRightFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框顶部位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框顶部位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetTopFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框底部位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框底部位置。
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetBottomFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框方向。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @param int 文本框的索引。
 * @return 返回文本框方向。
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetTextDirectionFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief 获取文本框数量大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox 指向OH_Drawing_TextBox对象的指针，由{@link OH_Drawing_TypographyGetRectsForRange}或
 * {@link OH_Drawing_TypographyGetRectsForPlaceholders}获取。
 * @return 返回文本框数量大小。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetSizeOfTextBox(OH_Drawing_TextBox*);

/**
 * @brief 获取坐标处文本的索引位置和亲和性。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param double 光标的x坐标。
 * @param double 光标的y坐标。
 * @return 返回坐标处字体的索引位置和亲和性，返回类型为{@link OH_Drawing_PositionAndAffinity}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinate(OH_Drawing_Typography*,
    double, double);

/**
 * @brief 获取坐标处文本所属字符簇的索引位置和亲和性，字符簇指一个或多个字符组成的整体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param double 光标的x坐标。
 * @param double 光标的y坐标。
 * @return 返回坐标处指定类型字体的索引位置和亲和性，返回类型为{@link OH_Drawing_PositionAndAffinity}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster(OH_Drawing_Typography*,
    double, double);

/**
 * @brief 获取OH_Drawing_PositionAndAffinity对象的位置属性。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity 指向OH_Drawing_PositionAndAffinity对象的指针，
 * 由{@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate}或
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}获取。
 * @return 返回OH_Drawing_PositionAndAffinity对象的位置属性。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetPositionFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief 获取OH_Drawing_PositionAndAffinity对象的亲和性，根据亲和性可判断字体会靠近前方文本还是后方文本。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity 指向OH_Drawing_PositionAndAffinity对象的指针，
 * 由{@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate}或
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}获取。
 * @return 返回OH_Drawing_PositionAndAffinity对象的亲和性。
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetAffinityFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief 获取单词的边界。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param size_t 单词索引。
 * @return 返回单词边界，返回类型为{@link OH_Drawing_Range}结构体。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetWordBoundary(OH_Drawing_Typography*, size_t);

/**
 * @brief 获取OH_Drawing_Range对象开始位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range 指向OH_Drawing_Range对象的指针，由{@link OH_Drawing_TypographyGetWordBoundary}获取。
 * @return 返回OH_Drawing_Range对象开始位置。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetStartFromRange(OH_Drawing_Range*);

/**
 * @brief 获取OH_Drawing_Range对象结束位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range 指向OH_Drawing_Range对象的指针，由{@link OH_Drawing_TypographyGetWordBoundary}获取。
 * @return 返回OH_Drawing_Range对象结束位置。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetEndFromRange(OH_Drawing_Range*);

/**
 * @brief 获取文本行数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回行数。
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_TypographyGetLineCount(OH_Drawing_Typography*);

/**
 * @brief 设置文本装饰样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置的文本装饰样式，支持可选的装饰样式具体可见{@link OH_Drawing_TextDecorationStyle}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationStyle(OH_Drawing_TextStyle*, int);

/**
 * @brief 设置文本装饰线的厚度缩放比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 缩放比例。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationThicknessScale(OH_Drawing_TextStyle*, double);

/**
 * @brief 设置文本的字符间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 间距大小。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLetterSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief 设置文本的单词间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param double 间距大小。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleWordSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief 设置文本为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param bool 设置一半行间距是否生效，true表示生效，false表示不生效。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleHalfLeading(OH_Drawing_TextStyle*, bool);

/**
 * @brief 设置文本的省略号内容。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param char* 设置省略号内容，数据类型为指向char的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsis(OH_Drawing_TextStyle*, const char*);

/**
 * @brief 设置文本的省略号样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向OH_Drawing_TextStyle对象的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 设置省略号样式，支持可选的省略号样式具体可见{@link OH_Drawing_EllipsisModal}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsisModal(OH_Drawing_TextStyle*, int);

/**
 * @brief 设置文本的中断策略。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置中断策略，支持可选的中断策略具体可见{@link OH_Drawing_BreakStrategy}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextBreakStrategy(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置单词的断词方式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置断词方式，支持可选的断词方式样式具体可见{@link OH_Drawing_WordBreakType}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextWordBreakType(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置文本的省略号样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向OH_Drawing_TypographyStyle对象的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置省略号样式，支持可选的省略号样式样式具体可见{@link OH_Drawing_EllipsisModal}枚举。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsisModal(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置省略号样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param char 省略号样式。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsis(OH_Drawing_TypographyStyle* style, const char* ellipsis);

/**
 * @brief 获取指定行的行高
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 要指定的行数。
 * @return 返回指定行的行高。
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineHeight(OH_Drawing_Typography*, int);

/**
 * @brief 获取指定行的行宽。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向OH_Drawing_Typography对象的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 要指定的行数。
 * @return 返回指定行的行宽。
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineWidth(OH_Drawing_Typography*, int);

/**
 * @brief 设置文本划分比率。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param float 文本划分比率。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextSplitRatio(OH_Drawing_TypographyStyle* style, float textSplitRatio);

/**
 * @brief 获取文本是否有最大行数限制。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本是否有最大行数限制，true表示有最大行数限制，false表示无最大行数限制。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyIsLineUnlimited(OH_Drawing_TypographyStyle* style);

/**
 * @brief 获取文本是否有省略号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @return 返回文本是否有省略号，true表示有省略号，false表示无省略号。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyIsEllipsized(OH_Drawing_TypographyStyle* style);

/**
 * @brief 设置文本位置。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param char 文本位置。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLocale(OH_Drawing_TypographyStyle* style, const char* locale);

/**
 * @brief 获取文本字体属性。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_Font_Metrics 指向字体属性对象{@link OH_Drawing_Font_Metrics}的指针，由{@link OH_Drawing_Font_Metrics}获取。
 * @return 是否获取到字体属性，true表示获取到字体属性，false表示未获取到字体属性。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleGetFontMetrics(OH_Drawing_Typography*, OH_Drawing_TextStyle*, OH_Drawing_Font_Metrics*);

/**
 * @brief 设置文本类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextStyle(OH_Drawing_TypographyStyle*,OH_Drawing_TextStyle*);

/**
 * @brief 构造字体描述对象，用于描述系统字体详细信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 返回指向已创建的字体描述对象{@link OH_Drawing_FontDescriptor}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_CreateFontDescriptor(void);

/**
 * @brief 释放字体描述对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
* @param OH_Drawing_FontDescriptor 指向字体描述对象{@link OH_Drawing_FontDescriptor}的指针，由{@link OH_Drawing_CreateFontDescriptor}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyFontDescriptor(OH_Drawing_FontDescriptor*);

/**
 * @brief 构造字体解析对象，用于解析系统字体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 返回指向已创建的字体解析对象{@link OH_Drawing_FontParser}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontParser* OH_Drawing_CreateFontParser(void);

/**
 * @brief 释放字体解析对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser 指向字体解析对象{@link OH_Drawing_FontParser}的指针，由{@link OH_Drawing_CreateFontParser}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyFontParser(OH_Drawing_FontParser*);

/**
 * @brief 获取系统字体名称列表。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser 指向字体解析对象{@link OH_Drawing_FontParser}的指针，由{@link OH_Drawing_CreateFontParser}获取。
 * @param size_t 返回获取到的系统字体名称数量。
 * @return 返回获取到的系统字体列表。
 * @since 12
 * @version 1.0
 */
char** OH_Drawing_FontParserGetSystemFontList(OH_Drawing_FontParser*, size_t*);

/**
 * @brief 释放系统字体名称列表占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param char** 指向系统字体名称列表的指针。
 * @param size_t* 系统字体名称列表的数量。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroySystemFontList(char**, size_t);

/**
 * @brief 根据传入的系统字体名称获取系统字体的相关信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser 指向字体解析对象{@link OH_Drawing_FontParser}的指针，由{@link OH_Drawing_CreateFontParser}获取。
 * @param char* 系统字体名。
 * @return 返回系统字体。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_FontParserGetFontByName(OH_Drawing_FontParser*, const char*);

/**
 * @brief 获取行位置信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @return 返回指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_LineMetrics* OH_Drawing_TypographyGetLineMetrics(OH_Drawing_Typography*);

/**
 * @brief 获取行数量。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @return 返回行数量。
 * @since 12
 * @version 1.0
 */
size_t OH_Drawing_LineMetricsGetSize(OH_Drawing_LineMetrics*);

/**
 * @brief 释放行位置信息对象占用的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyLineMetrics(OH_Drawing_LineMetrics*);

/**
 * @brief 获取指定行位置信息对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 要获取的行数。
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @return 行位置信息对象是否成功获取，true表示成功获取，false表示未成功获取。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyGetLineMetricsAt(OH_Drawing_Typography*, int, OH_Drawing_LineMetrics*);

/**
 * @brief 获取指定行的位置信息或指定行第一个字符的位置信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 行号。
 * @param bool true为获取整行的位置信息，false为获取第一个字符的位置信息。
 * @param bool 文字宽度是否包含空白符，true表示不包含空白符，false表示包含空白符。
 * @param OH_Drawing_LineMetrics 指向行位置信息对象{@link OH_Drawing_LineMetrics}的指针，由{@link OH_Drawing_LineMetrics}获取。
 * @return 指定行的行位置信息或第一个字符的位置信息是否成功获取，true表示成功获取，false表示未成功获取。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyGetLineInfo(OH_Drawing_Typography*, int, bool, bool, OH_Drawing_LineMetrics*);

/**
 * @brief 设置文本排版字重。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字重，设置0字重为thin，设置1字重为extra-light，设置2字重为light，设置4字重为medium，设置5字重为semi-bold，
 * 设置6字重为bold，设置7字重为extra-bold，设置8字重为black，设置3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontWeight(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置字体风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字体风格，设置1为斜体，设置0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontStyle(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置字体家族的名称。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param char 字体家族的名称，数据类型为指向char的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontFamily(OH_Drawing_TypographyStyle*, const char*);

/**
 * @brief 设置文本排版字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字号（大于0）。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontSize(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版字体高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字体高度。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontHeight(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版是否为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置一半行间距是否生效，true表示生效，false表示不生效。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextHalfLeading(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 设置文本排版是否启用行样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置行样式是否启用，true表示启用，false表示不启用。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextUseLineStyle(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 设置文本排版行样式字重。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字重，设置0字重为thin，设置1字重为extra-light，设置2字重为light，设置4字重为medium，设置5字重为semi-bold，
 * 设置6字重为bold，设置7字重为extra-bold，设置8字重为black，设置3或其它字重为normal/regular，具体可见{@link OH_Drawing_FontWeight}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontWeight(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置文本排版行样式风格。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 设置字体风格，设置1为斜体，设置0或其它为非斜体，具体可见{@link OH_Drawing_FontStyle}枚举。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontStyle(OH_Drawing_TypographyStyle*, int);

/**
 * @brief 设置文本排版行样式字体类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param int 字体名称数量。
 * @param char 指向字体类型的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontFamilies(OH_Drawing_TypographyStyle*,
    int, const char* fontFamilies[]);

/**
 * @brief 设置文本排版行样式字号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字号（大于0）。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontSize(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版行样式字体高度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 字体高度。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontHeight(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版行样式是否为一半行间距。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置一半行间距是否生效，true表示生效，false表示不生效。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleHalfLeading(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 设置文本排版行样式间距比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param double 间距比例。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleSpacingScale(OH_Drawing_TypographyStyle*, double);

/**
 * @brief 设置文本排版是否仅启用行样式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle 指向文本风格对象{@link OH_Drawing_TypographyStyle}的指针，由{@link OH_Drawing_CreateTypographyStyle}获取。
 * @param bool 设置仅启用行样式是否生效，true表示生效，false表示不生效。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleOnly(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief 创建指向字体阴影对象的指针。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 指向创建的字体阴影对象。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_CreateTextShadow(void);

/**
 * @brief 释放被字体阴影对象占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyTextShadow(OH_Drawing_TextShadow*);

/**
 * @brief 获取字体阴影容器。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return 返回指向字体阴影容器{@link OH_Drawing_TextShadow}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_TextStyleGetShadows(OH_Drawing_TextStyle*);

/**
 * @brief 获取字体阴影容器的大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @return int 返回字体阴影容器的大小。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_TextStyleGetShadowCount(OH_Drawing_TextStyle*);

/**
 * @brief 字体阴影容器中添加字体阴影元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleAddShadow(OH_Drawing_TextStyle*, const OH_Drawing_TextShadow*);

/**
 * @brief 清除字体阴影容器中的所有元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleClearShadows(OH_Drawing_TextStyle*);

/**
 * @brief 根据下标获取字体阴影容器中的元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle 指向字体风格对象{@link OH_Drawing_TextStyle}的指针，由{@link OH_Drawing_CreateTextStyle}获取。
 * @param int 下标索引。
 * @return 返回指向字体阴影对象{@link OH_Drawing_TextShadow}的指针。
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_TextStyleGetShadowWithIndex(OH_Drawing_TextStyle*, int);

/**
 * @brief 设置文本的排版缩进。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 排版缩进数量。
 * @param float 指向缩进类型的指针。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographySetIndents(OH_Drawing_Typography*, int, const float indents[]);

/**
 * @brief 根据下标获取排版缩进容器中的元素。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 下标索引。
 * @return float 返回索引对应的元素值。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_TypographyGetIndentsWithIndex(OH_Drawing_Typography*, int);

/**
 * @brief 获取行的边界。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography 指向文本对象{@link OH_Drawing_Typography}的指针，由{@link OH_Drawing_CreateTypography}获取。
 * @param int 行索引
 * @param bool 设置返回的边界是否包含空格，true为包含空格，false为不包含空格。
 * @return 返回指向行边界对象的指针{@link OH_Drawing_Range}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetLineTextRange(OH_Drawing_Typography*, int, bool);

/**
 * @brief 释放由被字体阴影对象OH_Drawing_TextShadow构成的vector占据的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow 指向字体阴影对象{@link OH_Drawing_TextShadow}的指针，由{@link OH_Drawing_CreateTextShadow}获取。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyTextShadows(OH_Drawing_TextShadow*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif