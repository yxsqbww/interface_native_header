/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_IMAGE_H_
#define NDK_INCLUDE_NATIVE_IMAGE_H_

/**
 * @addtogroup OH_NativeImage
 * @{
 *
 * @brief 提供NativeImage功能，作为数据消费者，主要用来将数据和OpenGL纹理对接，需在OpenGL环境下使用。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @since 9
 * @version 1.0
 */

/**
 * @file native_image.h
 *
 * @brief 定义获取和使用NativeImage的相关函数。
 *
 * 引用文件<native_image/native_image.h>
 * @library libnative_image.so
 * @since 9
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供OH_NativeImage结构体声明。
 * @since 9
 */
struct OH_NativeImage;

/**
 * @brief 提供OH_NativeImage结构体声明。
 * @since 9
 */
typedef struct OH_NativeImage OH_NativeImage;

/**
 * @brief 提供对NativeWindow的访问功能。
 * @since 9
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief 有buffer可获取时触发的回调函数。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param context 用户自定义的上下文信息，会在回调触发时返回给用户。
 * @since 11
 * @version 1.0
 */
typedef void (*OH_OnFrameAvailable)(void *context);

/**
 * @brief 一个OH_NativeImage的监听者，通过{@Link OH_NativeImage_SetOnFrameAvailableListener}接口注册
 * 该监听结构体，当有buffer可获取时，将触发回调给用户。
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_OnFrameAvailableListener {
    /** 用户自定义的上下文信息，会在回调触发时返回给用户 */
    void *context;
    /** 有buffer可获取时触发的回调函数 */
    OH_OnFrameAvailable onFrameAvailable;
} OH_OnFrameAvailableListener;

/**
 * @brief 创建一个<b>OH_NativeImage</b>实例，该实例与OpenGL ES的纹理ID和纹理目标相关联。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param textureId OpenGL ES的纹理ID，OH_NativeImage实例会与之相关联。
 * @param textureTarget OpenGL ES的纹理目标。
 * @return 返回一个指向<b>OH_NativeImage</b>实例的指针。
 * returns <b>NULL</b> otherwise
 * @since 9
 * @version 1.0
 */
OH_NativeImage* OH_NativeImage_Create(uint32_t textureId, uint32_t textureTarget);

/**
 * @brief 获取与OH_NativeImage相关联的OHNativeWindow指针. 该OHNativeWindow后续不再需要时需要调用\n
 * OH_NativeWindow_DestroyNativeWindow释放。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @return 成功则返回一个指向OHNativeWindow实例的指针，否则返回<b>NULL</b>。
 * @since 9
 * @version 1.0
 */
OHNativeWindow* OH_NativeImage_AcquireNativeWindow(OH_NativeImage* image);

/**
 * @brief 将OH_NativeImage实例附加到当前OpenGL ES上下文, 且该OpenGL ES纹理会绑定到 \n
 * GL_TEXTURE_EXTERNAL_OES, 并通过OH_NativeImage进行更新。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @param textureId 是OH_NativeImage要附加到的OpenGL ES纹理的id。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeImage_AttachContext(OH_NativeImage* image, uint32_t textureId);

/**
 * @brief 将OH_NativeImage实例从当前OpenGL ES上下文分离。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 */

int32_t OH_NativeImage_DetachContext(OH_NativeImage* image);

/**
 * @brief 通过OH_NativeImage获取最新帧更新相关联的OpenGL ES纹理。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeImage_UpdateSurfaceImage(OH_NativeImage* image);

/**
 * @brief 获取最近调用OH_NativeImage_UpdateSurfaceImage的纹理图像的相关时间戳。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @return 返回纹理图像的相关时间戳。
 * @since 9
 * @version 1.0
 */
int64_t OH_NativeImage_GetTimestamp(OH_NativeImage* image);

/**
 * @brief 获取最近调用OH_NativeImage_UpdateSurfaceImage的纹理图像的变化矩阵。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @param matrix 用来存储要获取的4*4的变化矩阵。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeImage_GetTransformMatrix(OH_NativeImage* image, float matrix[16]);

/**
 * @brief 获取OH_NativeImage的surface编号。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @param surfaceId 是指向surface编号的指针。
 * @return 返回值为0表示执行成功。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeImage_GetSurfaceId(OH_NativeImage* image, uint64_t* surfaceId);

/**
 * @brief 设置帧可用回调。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @param listener 表示回调监听者。
 * @return 返回值为0表示执行成功。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeImage_SetOnFrameAvailableListener(OH_NativeImage* image, OH_OnFrameAvailableListener listener);

/**
 * @brief 取消设置帧可用回调。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @return 返回值为0表示执行成功。
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeImage_UnsetOnFrameAvailableListener(OH_NativeImage* image);

/**
 * @brief 销毁通过OH_NativeImage_Create创建的<b>OH_NativeImage</b>实例, 销毁后该\n
 * <b>OH_NativeImage</b>指针会被赋值为空。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeImage
 * @param image 是指向<b>OH_NativeImage</b>实例的指针。
 * @since 9
 * @version 1.0
 */
void OH_NativeImage_Destroy(OH_NativeImage** image);

#ifdef __cplusplus
}
#endif

/** @} */
#endif