/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIVIEWDFX_HIDEBUG_H
#define HIVIEWDFX_HIDEBUG_H
/**
 * @addtogroup HiDebug
 * @{
 *
 * @brief 提供调试功能。
 *
 * 本模块函数可用于获取 cpu uage、memory、heap、capture trace等。
 *
 * @since 12
 */

/**
 * @file hideug.h
 *
 * @brief 定义HiDebug模块的调试功能。
 *
 * @library libohhidebug.so
 * @syscap SystemCapability.HiviewDFX.HiProfiler.HiDebug
 * @since 12
 */

#include <stdint.h>
#include "hidebug_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 获取系统的CPU资源占用情况百分比。
 *
 * @return 返回系统CPU资源占用情况百分比。
 * @since 12
 */
double OH_HiDebug_GetSystemCpuUsage();

/**
 * @brief 获取进程的CPU使用率百分比。
 *
 * @return 返回进程的CPU使用率百分比。
 * @since 12
 */
double OH_HiDebug_GetAppCpuUsage();

/**
 * @brief 获取应用所有线程CPU使用情况。
 *
 * @return 返回所有线程CPU使用情况，见{@link HiDebug_ThreadCpuUsagePtr}。
 * @since 12
 */
HiDebug_ThreadCpuUsagePtr OH_HiDebug_GetAppThreadCpuUsage();

/**
 * @brief 获取应用程序所有线程的可用CPU使用缓冲区。
 *
 * @param threadCpuUsage 应用的所有线程可用CPU使用缓冲区指针，见{@link HiDebug_ThreadCpuUsagePtr}。
 * @since 12
 */
void OH_HiDebug_FreeThreadCpuUsage(HiDebug_ThreadCpuUsagePtr *threadCpuUsage);

/**
 * @brief 获取系统内存信息。
 *
 * @param systemMemInfo 表示指向{@link HiDebug_SystemMemInfo}。
 * @since 12
 */
void OH_HiDebug_GetSystemMemInfo(HiDebug_SystemMemInfo *systemMemInfo);

/**
 * @brief 获取应用程序进程的内存信息。
 *
 * @param nativeMemInfo 表示指向{@link HiDebug_NativeMemInfo}。
 * @since 12
 */
void OH_HiDebug_GetAppNativeMemInfo(HiDebug_NativeMemInfo *nativeMemInfo);

/**
 * @brief 获取应用程序进程的内存限制。
 *
 * @param memoryLimit 表示指向{@link HiDebug_MemoryLimit}。
 * @since 12
 */
void OH_HiDebug_GetAppMemoryLimit(HiDebug_MemoryLimit *memoryLimit);

/**
 * @brief 启动应用trace采集。
 *
 * @param flag 采集线程trace方式。
 * @param tags 采集trace场景标签。
 * @param limitSize trace文件的最大大小（以字节为单位），最大为 500MB。
 * @param fileName 输出trace文件名缓冲区。
 * @param length 输出trace文件名缓冲区长度。
 * @return 如果成功返回{@code HIDEBUG_SUCCESS}，见{@link HiDebug_ErrorCode}。
 * @since 12
 */
HiDebug_ErrorCode OH_HiDebug_StartAppTraceCapture(HiDebug_TraceFlag flag, uint64_t tags, uint32_t limitSize,
    char* fileName, uint32_t length);

/**
 * @brief 停止采集应用程序trace。
 *
 * @return 如果成功返回{@code HIDEBUG_SUCCESS}，见{@link HiDebug_ErrorCode}。
 * @since 12
 */
HiDebug_ErrorCode OH_HiDebug_StopAppTraceCapture();

#ifdef __cplusplus
}
#endif
/** @} */

#endif // HIVIEWDFX_HIDEBUG_H