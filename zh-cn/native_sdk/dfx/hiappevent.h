/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HIVIEWDFX_HIAPPEVENT_H
#define HIVIEWDFX_HIAPPEVENT_H
/**
 * @addtogroup HiAppEvent
 * @{
 *
 * @brief HiAppEvent模块提供应用事件打点功能。
 *
 * 为应用程序提供事件打点功能，记录运行过程中上报的故障事件、统计事件、安全事件和用户行为事件。基于事件信息，您可以分析应用的运行状态。
 *
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file hiappevent.h
 *
 * @brief HiAppEvent模块的应用事件打点函数定义。
 *
 * 在执行应用事件打点之前，您必须先构造一个参数列表对象来存储输入的事件参数，并指定事件领域、事件名称和事件类型。
 *
 * <p>事件领域：用于标识事件打点的领域的字符串。
 * <p>事件名称：用于标识事件打点的名称的字符串。
 * <p>事件类型：故障、统计、安全、行为。
 * <p>参数列表：用于存储事件参数的链表，每个参数由参数名和参数值组成。
 *
 * 示例代码:
 * 00 引入头文件:
 * <pre>
 *     #include "hiappevent/hiappevent.h"
 * </pre>
 * 01 创建一个参数列表指针：
 * <pre>
 *     ParamList list = OH_HiAppEvent_CreateParamList();
 * </pre>
 * 02 添加参数到参数列表中：
 * <pre>
 *     bool boolean = true;
 *     OH_HiAppEvent_AddBoolParam(list, "bool_key", boolean);
 *     int32_t nums[] = {1, 2, 3};
 *     OH_HiAppEvent_AddInt32ArrayParam(list, "int32_arr_key", nums, sizeof(nums) / sizeof(nums[0]));
 * </pre>
 * 03 执行事件打点：
 * <pre>
 *     int res = OH_HiAppEvent_Write("test_domain", "test_event", BEHAVIOR, list);
 * </pre>
 * 04 销毁参数列表指针，释放其分配内存：
 * <pre>
 *     OH_HiAppEvent_DestroyParamList(list);
 * </pre>
 *
 * @since 8
 * @version 1.0
 */

#include <stdbool.h>
#include <stdint.h>

#include "hiappevent_cfg.h"
#include "hiappevent_event.h"
#include "hiappevent_param.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 事件类型。
 *
 * 建议您根据不同的使用场景选择不同的事件类型。
 *
 * @since 8
 * @version 1.0
 */
enum EventType {
    /* 故障事件类型 */
    FAULT = 1,

    /* 统计事件类型 */
    STATISTIC = 2,

    /* 安全事件类型 */
    SECURITY = 3,

    /* 行为事件类型 */
    BEHAVIOR = 4
};

/**
 * @brief 事件参数列表节点。
 *
 * @since 8
 * @version 1.0
 */
typedef struct ParamListNode* ParamList;

/**
 * @brief 创建一个指向参数列表对象的指针。
 *
 * @return 指向参数列表对象的指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_CreateParamList(void);

/**
 * @brief 销毁一个指向参数列表对象的指针，释放其分配内存。
 *
 * @param list 参数列表对象指针。
 * @since 8
 * @version 1.0
 */
void OH_HiAppEvent_DestroyParamList(ParamList list);

/**
 * @brief 添加一个布尔参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param boolean 需要添加的布尔参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddBoolParam(ParamList list, const char* name, bool boolean);

/**
 * @brief 添加一个布尔数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param booleans 需要添加的布尔数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddBoolArrayParam(ParamList list, const char* name, const bool* booleans, int arrSize);

/**
 * @brief 添加一个int8_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int8_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt8Param(ParamList list, const char* name, int8_t num);

/**
 * @brief 添加一个int8_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int8_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt8ArrayParam(ParamList list, const char* name, const int8_t* nums, int arrSize);

/**
 * @brief 添加一个int16_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int16_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt16Param(ParamList list, const char* name, int16_t num);

/**
 * @brief 添加一个int16_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int16_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt16ArrayParam(ParamList list, const char* name, const int16_t* nums, int arrSize);

/**
 * @brief 添加一个int32_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int32_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt32Param(ParamList list, const char* name, int32_t num);

/**
 * @brief 添加一个int32_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int32_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt32ArrayParam(ParamList list, const char* name, const int32_t* nums, int arrSize);

/**
 * @brief 添加一个int64_t参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的int64_t参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt64Param(ParamList list, const char* name, int64_t num);

/**
 * @brief 添加一个int64_t数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的int64_t数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddInt64ArrayParam(ParamList list, const char* name, const int64_t* nums, int arrSize);

/**
 * @brief 添加一个float参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的float参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddFloatParam(ParamList list, const char* name, float num);

/**
 * @brief 添加一个float数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的float数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddFloatArrayParam(ParamList list, const char* name, const float* nums, int arrSize);

/**
 * @brief 添加一个double参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param num 需要添加的double参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddDoubleParam(ParamList list, const char* name, double num);

/**
 * @brief 添加一个double数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param nums 需要添加的double数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddDoubleArrayParam(ParamList list, const char* name, const double* nums, int arrSize);

/**
 * @brief 添加一个字符串参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param str 需要添加的字符串参数值。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddStringParam(ParamList list, const char* name, const char* str);

/**
 * @brief 添加一个字符串数组参数到参数列表中。
 *
 * @param list 需要添加参数的参数列表指针。
 * @param name 需要添加的参数名称。
 * @param strs 需要添加的字符串数组参数值。
 * @param arrSize 需要添加的参数数组大小。
 * @return 添加参数后的参数列表指针。
 * @since 8
 * @version 1.0
 */
ParamList OH_HiAppEvent_AddStringArrayParam(ParamList list, const char* name, const char * const *strs, int arrSize);

/**
 * @brief 实现对参数为列表类型的应用事件打点。
 *
 * 在应用事件打点前，该接口会先对该事件的参数进行校验。如果校验成功，则接口会将事件写入事件文件。
 *
 * @param domain 事件领域。您可以根据需要自定义事件领域。
 * @param name 事件名称。您可以根据需要自定义事件名称。
 * @param type 事件类型，在{@link EventType}中定义。
 * @param list 事件参数列表，每个参数由参数名和参数值组成。
 * @return 如果事件参数校验成功，则返回{@code 0}，将事件写入事件文件；如果事件中存在无效参数，则返回正值，丢弃
 * 无效参数后将事件写入事件文件；如果事件参数校验失败，则返回负值，并且事件将不会写入事件文件。
 * @since 8
 * @version 1.0
 */
int OH_HiAppEvent_Write(const char* domain, const char* name, enum EventType type, const ParamList list);

/**
 * @brief 实现应用事件打点的配置功能。
 *
 * 应用事件打点配置接口，用于配置事件打点开关、事件文件目录存储配额大小等功能。
 *
 * @param name 配置项名称。
 * @param value 配置项值。
 * @return 配置结果。
 * @since 8
 * @version 1.0
 */
bool OH_HiAppEvent_Configure(const char* name, const char* value);

#ifdef __cplusplus
}
#endif
/** @} */
#endif // HIVIEWDFX_HIAPPEVENT_H