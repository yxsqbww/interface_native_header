/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Sensor
 * @{
 *
 * @brief 提供API来使用常见的传感器特性。例如，调用API来获取传感器和订阅或取消订阅传感器数据。
 * @since 11
 */
/**
 * @file oh_sensor.h
 *
 * @brief 声明操作传感器的API，包括获取传感器信息和订阅取消订阅传感器数据。
 * @library libohsensor.so
 * @syscap SystemCapability.Sensors.Sensor
 * @since 11
 */

#ifndef OH_SENSOR_H
#define OH_SENSOR_H

#include "oh_sensor_type.h"

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 获取设备上所有传感器的信息。
 *
 * @param infos - 双指针指向设备上所有传感器的信息。请参考{@link Sensor_Info}。
 * @param count - 指向设备上传感器数量的指针。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 *
 * @since 11
 */
Sensor_Result OH_Sensor_GetInfos(Sensor_Info **infos, uint32_t *count);

/**
 * @brief 订阅传感器数据。系统将以指定的频率向用户报告传感器数据。
 * 订阅加速度传感器，需要申请ohos.permission.ACCELEROMETER权限；
 * 订阅陀螺仪传感器，需要申请ohos.permission.GYROSCOPE权限；
 * 订阅计步器相关传感器时，需要申请ohos.permission.ACTIVITY_MOTION权限；
 * 订阅与健康相关的传感器时，比如心率传感器，需要申请ohos.permission.READ_HEALTH_DATA权限，否则订阅失败。
 * 订阅其余传感器不需要申请权限。
 *
 * @param id - 指向传感器订阅ID的指针。请参考{@link Sensor_SubscriptionId}。
 * @param attribute - 指向订阅属性的指针，该属性用于指定数据报告频率。请参考{@link Sensor_SubscriptionAttribute}。
 * @param subscriber - 指向订阅者信息的指针，该信息用于指定的回调函数报告传感器数据。请参考{@link Sensor_Subscriber}。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @permission ohos.permission.ACCELEROMETER or ohos.permission.GYROSCOPE or
 *             ohos.permission.ACTIVITY_MOTION or ohos.permission.READ_HEALTH_DATA
 * @since 11
 */
Sensor_Result OH_Sensor_Subscribe(const Sensor_SubscriptionId *id,
    const Sensor_SubscriptionAttribute *attribute, const Sensor_Subscriber *subscriber);

/**
 * @brief 取消订阅传感器数据。
 * 取消订阅加速度计传感器，需要申请ohos.permission.ACCELEROMETER权限；
 * 取消订阅陀螺仪传感器，需要申请ohos.permission.GYROSCOPE权限；
 * 取消订阅计步器相关传感器时，需要申请ohos.permission.ACTIVITY_MOTION权限；
 * 取消订阅与健康相关的传感器时，需要申请ohos.permission.READ_HEALTH_DATA权限，否则取消订阅失败。
 * 取消订阅其余传感器不需要申请权限。
 *
 * @param id - 指向传感器订阅ID的指针。请参考{@link Sensor_SubscriptionId}。
 * @param subscriber - 指向订阅者信息的指针，该信息用于指定的回调函数报告传感器数据。请参考{@link Sensor_Subscriber}。
 * @return 如果操作成功返回<b>SENSOR_SUCCESS</b>；否则返回{@link Sensor_Result}中定义的错误代码。
 * @permission ohos.permission.ACCELEROMETER or ohos.permission.GYROSCOPE or
 *             ohos.permission.ACTIVITY_MOTION or ohos.permission.READ_HEALTH_DATA
 *
 * @since 11
 */
Sensor_Result OH_Sensor_Unsubscribe(const Sensor_SubscriptionId *id, const Sensor_Subscriber *subscriber);
#ifdef __cplusplus
}
#endif
#endif // OH_SENSOR_H