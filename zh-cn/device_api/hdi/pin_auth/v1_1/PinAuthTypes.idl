/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 4.0
 */

/**
 * @file PinAuthTypes.idl
 *
 * @brief 定义口令认证驱动的枚举类和数据结构。
 *
 * @since 4.0
 */

package ohos.hdi.pin_auth.v1_1;

/**
 * @brief 获取执行器属性信息。
 *
 * @since 4.0
 * @version 1.1
 */
enum GetPropertyType : int {
    /** 获取执行器的认证子类型。 */
    AUTH_SUB_TYPE = 1,
    /** 获取执行器的剩余锁定时间。 */
    LOCKOUT_DURATION = 2,
    /** 获取执行器的剩余可重试次数。 */
    REMAIN_ATTEMPTS = 3
};

/**
 * @brief 执行器属性。
 *
 * @since 4.0
 * @version 1.1
 */
struct Property {
    /** 认证子类型。 */
    unsigned long authSubType;
    /** 剩余锁定时间。 */
    int lockoutDuration;
    /** 剩余可重试次数。 */
    int remainAttempts;
};
/** @} */